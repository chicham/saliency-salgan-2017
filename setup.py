from setuptools import setup, find_packages


setup(name="salgan",
      version="0.1",
      packages=find_packages(),
      scripts=['scripts/server_salgan.py'])

