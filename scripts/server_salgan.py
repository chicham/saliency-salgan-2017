import os
import numpy as np
import cv2
import glob
import argparse
from salgan.utils import *
from salgan.constants import *
from salgan.models.model_bce import ModelBCE
from path import Path
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square

from PIL import Image
from StringIO import StringIO

import tornado.ioloop
from tornado.options import define, options, parse_command_line
from tornado.web import Application, RequestHandler
from tornado import gen, httpclient

import json

define("port", default=6000, help="run on the given port", type=int)
define("debug", default=False, help="run in debug mode")

def test_image(img, path_output, model_to_test=None):
    saliency_map = predict(model=model_to_test,
                           image_stimuli=img,
                           path_output_maps=path_output)
    return saliency_map


def load_model():
    model = ModelBCE(INPUT_SIZE[0], INPUT_SIZE[1], batch_size=8)
    load_weights(model.net['output'], path='gen_', epochtoload=90)
    return model


def boundingbox(image):
    # apply threshold
    thresh= threshold_otsu(image)
    bw = closing(image > thresh, square(3))
    cleared = clear_border(bw)

    label_image = label(cleared)
    rows, cols = image.shape[:2]

    for region in regionprops(label_image):
        if region.area >= 100:
            minr, minc, maxr, maxc = region.bbox
            minr = float(minr) / rows
            minc = float(minc) / cols
            maxr = float(maxr) / rows
            maxc = float(maxc) / cols
            centr = (maxr-minr) / 2
            centc = (maxc-minc) / 2
            yield {'topleft': {'row': minr, 'col': minc},
                   'bottomright': {'row': maxr, 'col': maxc},
                   'center': {'row': centr, 'col': centc},
                   'width': cols,
                   'height': rows}


class MainHandler(RequestHandler):
    def initialize(self, model):
        self.model = model

    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body.decode('utf-8'))
        if 'url' in data:
            client = httpclient.AsyncHTTPClient()
            response = yield client.fetch(data['url'])
            image = np.array(Image.open(StringIO(response.body)))
            saliency = test_image(image, path_output='.', model_to_test=self.model)
            points = tuple(boundingbox(saliency))
            self.write(json.dumps(points))


def main():
    parse_command_line()
    model = load_model()
    print('Weights loaded')
    app = Application([
                        (r'/', MainHandler, dict(model=model)),
                      ],
                      debug=options.debug,)

    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
