import argparse
from path import Path
import json
from skimage import io
from PIL import Image, ImageDraw


def main(input, host="localhost", port=6000):
    # context = zmq.Context()
    # socket = context.socket(zmq.REQ)
    # socket.connect ("tcp://%s:%s" % (host, port))
    # socket.send_string(input)
    # message = socket.recv()
    return message


if __name__ == "__main__":
    # Url de l'image en entrée
    parser = argparse.ArgumentParser("Salgan Saliency")
    parser.add_argument("input", action="store", help="Path/Url to the image")
    parser.add_argument('-p', "--port", action="store", help="port of the server", default="6000")
    parser.add_argument('-a', "--address", action="store", help="address of the server", default="localhost")
    parser.add_argument('-o', "--output", action="store", help="Save output image")
    args = parser.parse_args()
    message = main(args.input, host=args.address, port=args.port)
    print(message)
    if args.output:
        output = Path(args.output).expand().abspath()
        img = Image.fromarray(io.imread(args.input))
        draw = ImageDraw.Draw(img)

