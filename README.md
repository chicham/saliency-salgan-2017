# Installation

```bash

> git clone https://chicham@gitlab.com/chicham/saliency-salgan-2017.git /path/to/salgan
> cd /path/to/salgan
> conda env create -f environment.yml
> source activate salgan-saliency
> pip install -e .
```

In order to run the scripts you MUST create a directory `data` into `salgan`
and put the following files in it:

- [gen_modelWeights0090.npz](https://imatge.upc.edu/web/sites/default/files/resources/1720/saliency/2017-salgan/gen_modelWeights0090.npz)
- [discrim_modelWeights0090.npz](https://imatge.upc.edu/web/sites/default/files/resources/1720/saliency/2017-salgan/discrim_modelWeights0090.npz)
- [vgg16.pkl](https://s3.amazonaws.com/lasagne/recipes/pretrained/imagenet/vgg16.pkl)


# Scripts

The package install two scripts in the path: `predict_saliency.py` and
`send_request.py`

The script `predict_saliency.py` allows to start the server that load the model and  `send_request.py` send path of the image to process.

```bash
> predict_saliency.py -o RESULT_DIR 
> send_request.py /path/to/image/
```

To use `salgan` with a gpu use the following commands

```bash
> THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32,lib.cnmem=1,optimizer_including=cudnn predict_saliency.py -o RESULT_DIR 
> send_request.py /path/to/image/
```

`send_request.py` returns the path to the result image which is
`RESULT_DIR/IMAGE_NAME.jpg`.

The script `send_request.py` can be copied and installed on another computer.
This script depends only on `path.py` and `pyzmq`.
